package com.luxoft.javatest.dao;

import com.luxoft.javatest.model.Option;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class OptionDAOImpl implements OptionDAO {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final String insertOptionString = "INSERT INTO OPTION(QUESTION_ID,CONTENT,CORRECT) VALUES (?,?,?)";
    private static final String removeOptionString = "DELETE FROM OPTION WHERE ID=?";
    private static final String getOptionsByQuestionIdString = "SELECT * FROM OPTION WHERE QUESTION_ID=?";
    
    @Override
    public void saveOption(Option option) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(getPreparedStatementCreator(option), keyHolder);
        option.setId((Long)keyHolder.getKey());
    }
    
    @Override
    public void removeOption(Option option) {
        jdbcTemplate.update(removeOptionString, new Object[] {option.getId()});       
    }
    
    @Override
    public List<Option> getOptionsByQuestionId(long questionId) {
        List<Option> options = new ArrayList<Option>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(getOptionsByQuestionIdString, new Object[] {questionId});
        for (Map row: rows) {
            long id = (Long)row.get("ID");
            String content = (String)row.get("CONTENT");
            boolean correct = (Boolean)row.get("CORRECT");
            Option option = new Option(content, correct);
            option.setId(id);
            option.setQuestionId(questionId);
            options.add(option);
        }
        return options;
    }
    
    private PreparedStatementCreator getPreparedStatementCreator(final Option option) {
        return new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection cnctn) throws SQLException {
                PreparedStatement ps = cnctn.prepareStatement(insertOptionString);
                ps.setLong(1, option.getQuestionId());
                ps.setString(2, option.getContent());
                ps.setBoolean(3, option.isCorrect());
                return ps;
            }            
        }; 
    }
    
    private RowMapper getRowMapper() {
        return new RowMapper() {
            @Override
            public Option mapRow(ResultSet rs, int i) throws SQLException {
                long id = rs.getLong("id");
                long questionId = rs.getLong("question_id");
                String content = rs.getString("content");
                boolean correct = rs.getBoolean("correct");
                Option option = new Option(content, correct);
                option.setId(id);
                option.setQuestionId(questionId);
                return option;
            }               
        };
    }
    
}

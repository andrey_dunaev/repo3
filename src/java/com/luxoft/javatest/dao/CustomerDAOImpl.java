package com.luxoft.javatest.dao;

import com.luxoft.javatest.exceptions.AuthorizationException;
import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.CustomerConfig;
import com.luxoft.javatest.model.Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private QuestionDAO questionDAO;
    private static final String updateCustomerString = "UPDATE CUSTOMER SET LOGIN=?, PASSWORD=?, EMAIL=?, RATING=?, ADMIN=? WHERE ID=?";
    private static final String insertCustomerString= "INSERT INTO CUSTOMER (LOGIN,PASSWORD,EMAIL,RATING,ADMIN) VALUES(?,?,?,?,?)";
    private static final String getCustomerByLoginString = "SELECT * FROM CUSTOMER WHERE LOGIN=?";
    private static final String getCustomerByLoginAndPasswordString = "SELECT * FROM CUSTOMER WHERE LOGIN=? AND PASSWORD=?";
    private static final String getCustomerByIdString = "SELECT * FROM CUSTOMER WHERE ID=?";
    private static final String updateCustomerRatingString = "UPDATE CUSTOMER SET RATING=RATING+? WHERE ID=?";
    private static final String getAllCustomersString = "SELECT * FROM CUSTOMER";
    
    @Override
    public void saveCustomer(Customer customer) {
        if (customer.getId() != 0) {
            jdbcTemplate.update(updateCustomerString, getPreparedStatementSetter(customer));
        } else {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(getPreparedStatementCreator(customer), keyHolder);
            customer.setId((Long)keyHolder.getKey());
        }
    }
    
    @Override
    public Customer getCustomer(String login) {
        try {
            Customer customer = (Customer)jdbcTemplate.queryForObject(getCustomerByLoginString, new Object[] {login}, getRowMapper());
            customer.setQuestions(questionDAO.getQuestionsByCustomerId(customer.getId()));
            return customer;
        } catch(EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
    @Override
    public Customer getCustomer(String login, String password) throws AuthorizationException {
        try {
            Customer customer = (Customer)jdbcTemplate.queryForObject(getCustomerByLoginAndPasswordString, new Object[] {login, password}, getRowMapper());
            customer.setQuestions(questionDAO.getQuestionsByCustomerId(customer.getId()));
            return customer;
        } catch(EmptyResultDataAccessException ex) {
            throw new AuthorizationException("Wrong login or password");
        }       
    }
    
    @Override
    public Customer getCustomer(long id) {
        try {
            Customer customer = (Customer)jdbcTemplate.queryForObject(getCustomerByIdString, new Object[] {id}, getRowMapper());
            customer.setQuestions(questionDAO.getQuestionsByCustomerId(customer.getId()));
            return customer;
        } catch(EmptyResultDataAccessException ex) {
            return null;
        }     
    }
       
    @Override
    public Set<Customer> getAllCustomers() {
        Set<Customer> customers = new TreeSet<Customer>(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                int result = 0;
                Customer c1 = (Customer)o1;
                Customer c2 = (Customer)o2;
                int r1 = c1.getRating();
                int r2 = c2.getRating();
                if (r1 != r2) {
                    return (-1)*(r1 - r2);
                } else {
                    return (c1.getLogin()).compareTo(c2.getLogin());
                }
            }          
        });
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(getAllCustomersString);
        for (Map row: rows) {
            Customer customer = new Customer(new CustomerConfig().setLogin((String)row.get("LOGIN")).setRating((Integer)row.get("RATING")));
            customers.add(customer);
        }
        return customers;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_COMMITTED)
    public void changeCustomerRating(long id, int change) {
        jdbcTemplate.update(updateCustomerRatingString, new Object[] {change, id});
    }
    
    private PreparedStatementSetter getPreparedStatementSetter(final Customer customer) {
        return new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1, customer.getLogin());
                ps.setString(2, customer.getPassword());
                ps.setString(3, customer.getEmail());
                ps.setInt(4, customer.getRating());
                ps.setBoolean(5, customer.isAdmin());
                if (customer.getId() != 0) {
                    ps.setLong(6, customer.getId());
                }
            }          
        };
    }
    
    private PreparedStatementCreator getPreparedStatementCreator(final Customer customer) {
        return new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection cnctn) throws SQLException {
                PreparedStatement ps = cnctn.prepareStatement(insertCustomerString);
                ps.setString(1, customer.getLogin());
                ps.setString(2, customer.getPassword());
                ps.setString(3, customer.getEmail());
                ps.setInt(4, customer.getRating());
                ps.setBoolean(5, customer.isAdmin());
                return ps;
            }            
        }; 
    }
    
    private RowMapper getRowMapper() {
        return new RowMapper() {
            @Override
            public Customer mapRow(ResultSet rs, int i) throws SQLException {
                long id = rs.getLong("id");
                String login = rs.getString("login");
                String password = rs.getString("password");
                String email = rs.getString("email");
                int rating = rs.getInt("rating");
                boolean admin = rs.getBoolean("admin");
                Customer customer = new Customer(new CustomerConfig().setLogin(login).setPassword(password).setEmail(email).setRating(rating));
                customer.setAdmin(admin);
                customer.setId(id);
                return customer;
            }               
        };
    }
    
}

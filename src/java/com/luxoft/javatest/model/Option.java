package com.luxoft.javatest.model;

public class Option {
    
    private long id;
    private long questionId;
    private String content;
    private boolean correct;
    private boolean chosen;
    
    public Option(String content) {
        this.content = content;
    }
    
    public Option(String content, boolean correct) {
        this.content = content;
        this.correct = correct;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }
    
}

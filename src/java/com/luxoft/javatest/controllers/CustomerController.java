package com.luxoft.javatest.controllers;

import com.luxoft.javatest.exceptions.AuthorizationException;
import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.CustomerConfig;
import com.luxoft.javatest.services.CustomerService;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomerController {
    
    @Autowired
    private CustomerService customerService;
    
    @RequestMapping(value = "login")
    public ModelAndView loginCustomer(HttpServletRequest req) {
        try {
            Customer customer = customerService.getCustomer((String)req.getParameter("userLogin"), (String)req.getParameter("userPassword"));
            req.getSession().setAttribute("currentCustomer", customer);
            return new ModelAndView("profile.jsp");
        } catch(AuthorizationException ex) {
            req.getSession().setAttribute("message", ex.getMessage());
            return new ModelAndView("error.jsp");
        }
    }
    
    @RequestMapping(value = "logout")
    public ModelAndView logoutCustomer(HttpServletRequest req) {
        req.getSession().invalidate();
        return new ModelAndView("home.jsp");
    }
    
    @RequestMapping(value = "register", method=RequestMethod.POST)
    public ModelAndView registerCustomer(HttpServletRequest req) {
        String login = req.getParameter("userLogin");
        String password = req.getParameter("userPassword");
        String email = req.getParameter("userEmail");
        if (customerService.getCustomer(login) != null) {
            req.getSession().setAttribute("message", "Client with such login already exists");
            return new ModelAndView("error.jsp");
        }
        Customer customer = new Customer(new CustomerConfig().setLogin(login).setPassword(password).setEmail(email).setRating(0));
        customerService.saveCustomer(customer);
        req.getSession().setAttribute("currentCustomer", customer);
        return new ModelAndView("profile.jsp");
    }
    
    @RequestMapping(value = "rating")
    public ModelAndView getRating(HttpServletRequest req) {
        Set<Customer> customers = customerService.getAllCustomers();
        req.getSession().setAttribute("customerRating", customers);
        return new ModelAndView("rating.jsp");
    }
    
}

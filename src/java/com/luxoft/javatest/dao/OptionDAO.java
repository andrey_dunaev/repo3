package com.luxoft.javatest.dao;

import com.luxoft.javatest.model.Option;
import java.util.List;

public interface OptionDAO {
    
    void saveOption(Option option);
    
    void removeOption(Option option);
    
    List<Option> getOptionsByQuestionId(long questionId);
    
}

<%-- 
    Document   : login
    Created on : Nov 10, 2014, 1:24:20 PM
    Author     : ADunaev
--%>

<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
        <style>
            <%@include file="css/style.css" %>
        </style>
        <script>
            <%@include file="js/jquery-1.11.0.min.js" %>
            <%@include file="js/javatest.js" %>
        </script>
        <title>Error page</title>
    </head>
    <body>
        <header>
            <div class="width">
                <h2>Java Test</h2>
                <div class="tagline">
                    <p>Test service for Java developers and students</p>
                </div>
            </div>
        </header>
        <section id="body" class="width clear">
            <aside id="sidebar" class="column-left">
                <c:choose>
                    <c:when test="${currentCustomer != null && currentCustomer.admin}">
                        <ul>
                            <li>
                                <h4>Administration</h4>
                                <ul class="blocklist">
                                    <li><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></li>
                                </ul>
                            </li>	
                        </ul>
                    </c:when>
                </c:choose>               
                <ul>
                    <li>
                        <h4>Navigate</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/home.jsp">Home</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/profile.jsp">My profile</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/test_choose.jsp">Tests</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/question.jsp">Add question</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/rating">Rating</a></li>
                        </ul>
                    </li>	
                </ul>
                <ul>
                    <li>
                        <h4>System</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/login.jsp">Log in</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/registration.jsp">Registration</a></li>
                        </ul>
                    </li>	
                </ul>
            </aside>
            <section id="content" class="column-right">   
                <div style="margin-top: 60pt; text-align: center;">
                    <h5>${message}</h5>
                    <c:choose>
                        <c:when test="${currentCustomer == null}">
                            <div  style="margin-top: 15pt; font-size: 1.1em;">
                                <a href="login.jsp">Log in system</a>
                            </div>
                            <div  style="margin-top: 15pt; font-size: 1.1em;">
                                <a href="registration.jsp">Register in system</a>
                            </div>
                        </c:when>
                    </c:choose>
                </div>
            </section>
        </section>
    </body>
</html>

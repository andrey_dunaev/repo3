package com.luxoft.javatest.services;

import com.luxoft.javatest.dao.QuestionDAO;
import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.Question;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService {
    
    @Autowired
    private QuestionDAO questionDAO;
    
    @Override
    public void saveQuestion(Customer customer, Question question) {
        customer.addQuestion(question);
        question.setAuthorId(customer.getId());
        questionDAO.saveQuestion(question);
    }
    
    @Override
    public void confirmQuestion(Question question) {
        questionDAO.confirmQuestion(question);
    }
    
    @Override
    public void confirmQuestion(long id) {
        questionDAO.confirmQuestion(questionDAO.getQuestionById(id));
    }
    
    @Override
    public void checkAnsweredQuestion(Customer customer, Question question) {
        questionDAO.checkAnsweredQuestion(customer, question);
    }
    
    @Override
    public void removeQuestion(Question question) {
        questionDAO.removeQuestion(question);
    }
    
    @Override
    public Question getQuestionById(long id) {
        return questionDAO.getQuestionById(id);
    }
    
    @Override
    public List<Question> getQuestionsByCustomerId(long customerId) {
        return questionDAO.getQuestionsByCustomerId(customerId);
    }
          
    @Override
    public List<Long> getAnsweredQuestions(long customerId) {
        return questionDAO.getAnsweredQuestions(customerId);
    }
    
    @Override
    public List<Question> getQuestionsForTest(long customerId, int level) {
        return questionDAO.getQuestionsForTest(customerId, level);
    }

    @Override
    public List<Question> getConfirmedQuestions() {
        return questionDAO.getConfirmedQuestions();
    }

    @Override
    public List<Question> getTemporaryQuestions() {
        return questionDAO.getTemporaryQuestions();
    }
    
    @Override
    public List<Question> getAllQuestions() {
        return questionDAO.getAllQuestions();
    }
    
    @Override
    public void addQuestions(List<Question> questions) {
        for (Question question: questions) {
            question.setTemporary(false);
            questionDAO.saveQuestion(question);
        }
    }
    
    @Override
    public void changeQuestionRating(long id, int change) {
        questionDAO.changeQuestionRating(id, change);
    }
    
}

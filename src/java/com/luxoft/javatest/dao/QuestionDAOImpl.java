package com.luxoft.javatest.dao;

import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.Option;
import com.luxoft.javatest.model.Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class QuestionDAOImpl implements QuestionDAO {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired 
    private OptionDAO optionDAO;
    private static final String updateQuestionString = "UPDATE QUESTION SET RATING=? WHERE ID=?";
    private static final String confirmQuestionString = "UPDATE QUESTION SET TEMPORARY=FALSE WHERE ID=?";
    private static final String insertQuestionString = "INSERT INTO QUESTION (AUTHOR_ID,CONTENT,LEVEL,RATING,TEMPORARY) VALUES (?,?,?,?,?)";
    private static final String updateAnsweredQuestionString = "INSERT INTO ANSWERED_QUESTION(CUSTOMER_ID, QUESTION_ID) VALUES(?,?)";
    private static final String removeQuestionString = "DELETE FROM QUESTION WHERE ID=?";
    private static final String getQuestionByIdString = "SELECT * FROM QUESTION WHERE ID=?";
    private static final String getQuestionsByCustomerIdString = "SELECT * FROM QUESTION WHERE AUTHOR_ID=?";
    private static final String getQuestionsForTestString = "SELECT * FROM QUESTION WHERE LEVEL=? AND AUTHOR_ID NOT IN(?) AND TEMPORARY=FALSE";
    private static final String getAnsweredQuestionsString = "SELECT * FROM ANSWERED_QUESTION WHERE CUSTOMER_ID=?";
    private static final String getConfirmedQuestionsString = "SELECT * FROM QUESTION WHERE TEMPORARY=FALSE";
    private static final String getTemporaryQuestionsString = "SELECT * FROM QUESTION WHERE TEMPORARY=TRUE";
    private static final String getAllQuestionsString = "SELECT * FROM QUESTION";
    private static final String updateQuestionRatingString = "UPDATE QUESTION SET RATING=RATING+? WHERE ID=?";
    
    @Override
    public void saveQuestion(Question question) {
        if (question.getId() != 0) {
            jdbcTemplate.update(updateQuestionString, new Object[] {question.getRating(), question.getId()});
        } else {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(getPreparedStatementCreator(question), keyHolder);
            question.setId((Long)keyHolder.getKey());
            for (Option option: question.getOptions()) {
                option.setQuestionId(question.getId());
                optionDAO.saveOption(option);
            }
        }
    }

    @Override
    public void confirmQuestion(Question question) {
        jdbcTemplate.update(confirmQuestionString, new Object[] {question.getId()});
    }
    
    @Override
    public void checkAnsweredQuestion(Customer customer, Question question) {
        jdbcTemplate.update(updateAnsweredQuestionString, new Object[] {customer.getId(), question.getId()});    
    }
    
    @Override
    public void removeQuestion(Question question) {
        for (Option option: question.getOptions()) {
            optionDAO.removeOption(option);
        }
        jdbcTemplate.update(removeQuestionString, new Object[] {question.getId()});      
    }
    
    @Override
    public Question getQuestionById(long id) {
        Question question = (Question)jdbcTemplate.queryForObject(getQuestionByIdString, new Object[] {id}, getRowMapper());
        question.setOptions(optionDAO.getOptionsByQuestionId(question.getId()));
        return question;
    }
    
    @Override
    public List<Question> getQuestionsByCustomerId(long customerId) {
        List<Question> questions = new ArrayList<Question>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(getQuestionsByCustomerIdString, customerId);
        for (Map row: rows) {
            Question question = parseRow(row);           
            questions.add(question);
        }
        return questions;
    }  
    
    public List<Long> getAnsweredQuestions(long customerId) {
        List<Long> answeredQuestions = new ArrayList<Long>();
        List<Map<String, Object>> answeredRows = jdbcTemplate.queryForList(getAnsweredQuestionsString, customerId);
        for (Map row: answeredRows) {          
            answeredQuestions.add((Long)row.get("QUESTION_ID"));
        }
        return answeredQuestions;
    }
    
    @Override
    public List<Question> getQuestionsForTest(long customerId, int level) {
        List<Question> questions = new ArrayList<Question>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(getQuestionsForTestString, new Object[] {level, customerId});
        for (Map row: rows) {
            Question question = parseRow(row);           
            questions.add(question);
        }
        List<Question> questionsForTest = new ArrayList<Question>();
        Collections.shuffle(questions);
        for (int i = 0; i < 10 && i < questions.size(); i++) {
            questionsForTest.add(questions.get(i));
            Collections.shuffle(questionsForTest.get(i).getOptions());
        }
        return questionsForTest;
    }
    
    @Override
    public List<Question> getConfirmedQuestions() {
        return getQuestions(getConfirmedQuestionsString);
    }
    
    @Override
    public List<Question> getTemporaryQuestions() {
        return getQuestions(getTemporaryQuestionsString);
    }
    
    @Override
    public List<Question> getAllQuestions() {
        return getQuestions(getAllQuestionsString);
    }
    
    private List<Question> getQuestions(String sqlString) {
        List<Question> questions = new ArrayList<Question>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sqlString);
        for (Map row: rows) {
            Question question = parseRow(row);           
            questions.add(question);
        }
        return questions;
    }
    
    @Override
    @Transactional(isolation=Isolation.READ_COMMITTED)
    public void changeQuestionRating(long id, int change) {
        jdbcTemplate.update(updateQuestionRatingString, new Object[] {change, id});
    }
    
    private PreparedStatementCreator getPreparedStatementCreator(final Question question) {
        return new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection cnctn) throws SQLException {
                PreparedStatement ps = cnctn.prepareStatement(insertQuestionString);
                ps.setLong(1, question.getAuthorId());
                ps.setString(2, question.getContent());
                ps.setInt(3, question.getLevel());
                ps.setInt(4, question.getRating());
                ps.setBoolean(5, question.isTemporary());
                return ps;
            }            
        }; 
    }
    
    private RowMapper getRowMapper() {
        return new RowMapper() {
            @Override
            public Question mapRow(ResultSet rs, int i) throws SQLException {
                long id = rs.getLong("id");
                long authorId = rs.getLong("author_id");
                String content = rs.getString("content");
                int level = rs.getInt("level");
                int rating = rs.getInt("rating");
                boolean temporary = rs.getBoolean("temporary");
                Question question = new Question(content, level, rating);
                question.setId(id);
                question.setAuthorId(authorId);
                question.setTemporary(temporary);
                return question;
            }               
        };
    }
    
    private Question parseRow(Map<String, Object> row) {
        long id = (Long)row.get("ID");
        long authorId = (Long)row.get("AUTHOR_ID");
        String content = (String)row.get("CONTENT");
        int level = (Integer)row.get("LEVEL");
        int rating = (Integer)row.get("RATING");
        boolean temporary = (Boolean)row.get("TEMPORARY");
        Question question = new Question(content, level, rating);
        question.setId(id);
        question.setAuthorId(authorId);
        question.setTemporary(temporary);
        question.setOptions(optionDAO.getOptionsByQuestionId(id));
        return question;
    }
    
}

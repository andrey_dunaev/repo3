package com.luxoft.javatest.exceptions;

public class RegistrationException  extends Exception {
    
    protected String message;
    
    public RegistrationException(String message) {
        this.message = message;
    }
    
    @Override
    public String getMessage() {
        return message;
    }
    
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function isNumeric() {
    var sum=$("#sum").val();
    if (!sum.match("^\\d{1,9}(\\.\\d{1,3})?$")) {
        $("#sumError").html("Incorrect sum");
        return false;
    }
    return true;
}
function checkLoginForm() {
    var correct = true;
    var login=$("#login").val();
    if (!login.match("^[a-zA-Z0-9_\\s]{5,50}$")) {
        $("#loginError").html("Incorrect login format");
        correct = false;
    } else {
        $("#loginError").html("");
    }
    var password=$("#password").val();
    if (!password.match("^[a-zA-Z0-9_\\s]{5,50}$")) {
        $("#passwordError").html("Incorrect password format");
        correct = false;
    } else {
        $("#passwordError").html("");
    }
    return correct;
}
function checkRegistrationForm() {
    var correct = true;
    var login=$("#login").val();
    if (!login.match("^[a-zA-Z0-9_\\s]{5,50}$")) {
        $("#loginError").html("Incorrect login format");
        correct = false;
    } else {
        $("#loginError").html("");
    }
    var password=$("#password").val();
    if (!password.match("^[a-zA-Z0-9_\\s]{5,50}$")) {
        $("#passwordError").html("Incorrect password format");
        correct = false;
    } else {
        $("#passwordError").html("");
    }
    var password2=$("#password2").val();
    if (password !== password2) {
        $("#passwordError2").html("Password doesn't match");
        correct = false;
    } else {
        $("#passwordError2").html("");
    }
    var email=$("#email").val();
    if (!email.match("^[a-zA-Z0-9\\-]{1,106}@[a-zA-Z0-9\\-]{1,10}.[a-zA-Z]{1,3}$")) {
        $("#emailError").html("Incorrect email format");
        correct = false;
    } else {
        $("#emailError").html("");
    }
    return correct;
}
function checkQuestionForm() {
    var correct = true;
    var questionText = $("#questionText").val();
    if (questionText === "") {
        $("#questionError").html("Empty question text");
        correct = false;
    } else {
        $("#questionError").html("");
    }
    var optionsCount = $("#optionsCount").val(); 
    var optionText;
    var correctOptionIsChosen = false;
    for (var i = 1; i <= optionsCount; i++) {
        optionText = $("#optionText" + i).val();
        if (optionText === "") {
            $("#optionError" + i).html("Empty option text");
            correct = false;
        } else {
            $("#optionError" + i).html("");
        }
        if (document.getElementById("option" + i).checked) {
            correctOptionIsChosen = true;
        }
    }
    if (correctOptionIsChosen === false) {
        $("#notChosenError").html("No option is chosen");
        correct = false;
    } else {
        $("#notChosenError").html("");
    }
    return correct;
}
function addOption() {
    var i = document.getElementById("optionsCount").getAttribute("value");
    if (i < 5) {
        var table = document.getElementById("table");
        var levelRow = document.getElementById("levelRow");
        var optionRow = document.createElement("TR");
        optionRow.innerHTML = "<td style=\"text-align:left;\">" +
                              "<input type=\"radio\" name=\"question\" id=\"option" + ++i + "\" value=\"" + i + "\" style=\"margin-right:10px;\"/>" +
                              "<textarea name=\"option" +  i + "\" maxlength=\"512\" id=\"optionText" + i + "\" rows=\"1\" cols=\"47\"></textarea>" + 
                              "</td><td id=\"optionError" + i + "\"class=\"error\"></td>";
        table.firstElementChild.insertBefore(optionRow, levelRow);
        document.getElementById("optionsCount").setAttribute("value", i);
    } else {
        $("#optionError" + i).html("5 options max");
    }
}
function changeRating(questionNumber, needToPlus) {
    var ratingChange = $("#ratingChange" + questionNumber).val();
    if (ratingChange === "0") {
        var rating = $("#rating" + questionNumber).val();
        if (needToPlus) {
            $("#rating" + questionNumber).val(++rating);
        } else {
            $("#rating" + questionNumber).val(--rating);
        }
        $("#ratingChange" + questionNumber).val(1);
    } else {
        $("#ratingError" + questionNumber).val("You have already rated this question");
    }
}
function changeAdminAction(formNumber) {
    var form = document.getElementById("form" + formNumber);
    form.action="remove";
}
function simple_timer(sec, block, direction) {
	var time = sec;
	direction = direction || false;			
	var hour = parseInt(time / 3600);
	if ( hour < 1 ) hour = 0;
	time = parseInt(time - hour * 3600);
	if ( hour < 10 ) hour = '0'+hour;
	var minutes	= parseInt(time / 60);
	if ( minutes < 1 ) minutes = 0;
	time = parseInt(time - minutes * 60);
	if ( minutes < 10 ) minutes = '0'+minutes;
	var seconds = time;
	if ( seconds < 10 ) seconds = '0'+seconds;
	block.innerHTML = hour+':'+minutes+':'+seconds;
	if ( direction ) {
            sec++;
            setTimeout(function(){ simple_timer(sec, block, direction); }, 1000);
	} else {
            sec--;
            if ( sec > 0 ) {
                setTimeout(function(){ simple_timer(sec, block, direction); }, 1000);
            } else {
                document.forms["form"].submit();
            }
	}
}
function start_countdown() {
    var block = document.getElementById('sample_countdown');
    simple_timer(600, block);
}


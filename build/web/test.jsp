<%-- 
    Document   : test
    Created on : 09.11.2014, 20:05:07
    Author     : Lenovo
--%>

<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
        <style>
            <%@include file="css/style.css" %>
        </style>
        <script>
            <%@include file="js/jquery-1.11.0.min.js" %>
            <%@include file="js/javatest.js" %>
        </script>
        <title>Test</title>      
    </head>
    <body onload="start_countdown()">
        <header>
            <div class="width">
                <h2>Java Test</h2>
                <div class="tagline">
                    <p>Test service for Java developers and students</p>
                </div>
            </div>
        </header>
        <section id="body" class="width clear">
            <aside id="sidebar" class="column-left">
                <c:choose>
                    <c:when test="${currentCustomer != null && currentCustomer.admin}">
                        <ul>
                            <li>
                                <h4>Administration</h4>
                                <ul class="blocklist">
                                    <li><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></li>
                                </ul>
                            </li>	
                        </ul>
                    </c:when>
                </c:choose>    
                <ul>
                    <li>
                        <h4>Navigate</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/home.jsp">Home</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/profile.jsp">My profile</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/test_choose.jsp">Tests</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/question.jsp">Add question</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/rating">Rating</a></li>
                        </ul>
                    </li>	
                </ul>
                <ul>
                    <li>
                        <h4>System</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/login.jsp">Log in</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/registration.jsp">Registration</a></li>
                        </ul>
                    </li>	
                </ul>
            </aside>
            <section id="content" class="column-right">               									
                <h2 style="text-align: center;">Java test (${levelString} level)</h2> 
                <div id="sample_countdown" style="font-size:20px; margin:0 auto; font-weight: bold;">00:10:00</div>
                <form action="checkResults" method="POST" name="form" id="form">
                    <c:forEach var="question" items="${questions}" varStatus="status">
                            <p>${status.count}. ${question.content}
                                <c:forEach var="option" items="${question.options}" varStatus="status2">
                                    <br><input type="radio" name="question${status.index}" value="${status2.index}" style="margin-right: 10px;" ${option.chosen? "checked" : ""}/>${option.content}                                                          
                                </c:forEach>
                                <br>Author: ${customers.get(status.index).login}
                                <br><input type="button" class="button" value="-" onClick="changeRating(${status.index}, false)"/>
                                <input type="text" name="rating${status.index}" id="rating${status.index}" value="${question.rating}" style="font-size:1.0em; text-align:center; width:4%; border-width:0; border-color: white;"/>
                                <input type="button" class="button" value="+" onClick="changeRating(${status.index}, true)"/>    
                                <input type="text" name="ratingError${status.index}" id="ratingError${status.index}" value="" class="error" style="font-size: 0.9em; margin-left: 5px; width: 30%; border-width:0; border-color: white;"/>
                                <input type="hidden" name="ratingChange${status.index}" id="ratingChange${status.index}" value="0"/>
                            </p>
                    </c:forEach>
                    <input type="hidden" name="questionsSize" value="${questions.size()}"/>
                    <input type="submit" class="button" value="Finish" style="font-size:1.2em; margin-top:25px;"/>
                </form>
            </section>
	</section>	
    </body>
</html>

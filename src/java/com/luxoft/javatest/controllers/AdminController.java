package com.luxoft.javatest.controllers;

import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.Question;
import com.luxoft.javatest.services.CustomerService;
import com.luxoft.javatest.services.QuestionService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {
    
    @Autowired
    private CustomerService customerService;
    @Autowired
    private QuestionService questionService;
    
    @RequestMapping(value="admin")
    public ModelAndView getTemporaryQuestions(HttpServletRequest req) {
        List<Question> questions = new ArrayList<Question>();
        questions = questionService.getTemporaryQuestions();
        List<Customer> customers = new ArrayList<Customer>();
        for (Question question: questions) {
            customers.add(customerService.getCustomer(question.getAuthorId()));
        }
        req.getSession().setAttribute("temporaryQuestions", questions);
        req.getSession().setAttribute("authors", customers);
        return new ModelAndView("admin.jsp");
    }
    
    @RequestMapping(value="confirm")
    public ModelAndView confirmQuestion(HttpServletRequest req) {
        int questionNumber = Integer.parseInt(req.getParameter("tempQuestion"));
        List<Question> questionsList = (List<Question>)req.getSession().getAttribute("temporaryQuestions");
        List<Customer> customersList = (List<Customer>)req.getSession().getAttribute("authors");
        questionService.confirmQuestion(questionsList.get(questionNumber));
        customerService.changeCustomerRating(customersList.get(questionNumber).getId(), questionsList.get(questionNumber).getLevel() * 3);
        questionsList.remove(questionNumber);
        customersList.remove(questionNumber);
        req.getSession().setAttribute("temporaryQuestions", questionsList);
        req.getSession().setAttribute("authors", customersList);
        return new ModelAndView("admin.jsp");
    }
    
    @RequestMapping(value="remove")
    public ModelAndView removeQuestion(HttpServletRequest req) {
        int questionNumber = Integer.parseInt(req.getParameter("tempQuestion"));
        List<Question> questionsList = (List<Question>)req.getSession().getAttribute("temporaryQuestions");
        List<Customer> customersList = (List<Customer>)req.getSession().getAttribute("authors");
        questionService.removeQuestion(questionsList.get(questionNumber));
        questionsList.remove(questionNumber);
        customersList.remove(questionNumber);
        req.getSession().setAttribute("temporaryQuestions", questionsList);
        req.getSession().setAttribute("authors", customersList);
        return new ModelAndView("admin.jsp");
    }
    
}

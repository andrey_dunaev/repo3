package com.luxoft.javatest.model;

public class CustomerConfig {
    
    private String login;
    private String password;
    private String email;
    private int rating;
    
    public CustomerConfig() {
        login = "";
        password = "";
        email = "";
        rating = 0;
    }

    public String getLogin() {
        return login;
    }

    public CustomerConfig setLogin(String login) {
        this.login = login;
        return this;
    }
    
    public String getPassword() {
        return password;
    }

    public CustomerConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CustomerConfig setEmail(String email) {
        this.email = email;
        return this;
    }

    public int getRating() {
        return rating;
    }

    public CustomerConfig setRating(int rating) {
        this.rating = rating;
        return this;
    }   
    
}

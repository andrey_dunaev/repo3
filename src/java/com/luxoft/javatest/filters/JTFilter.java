package com.luxoft.javatest.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class JTFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {        
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        HttpSession session = req.getSession();
        String path = req.getRequestURI();
        if (!path.contains("login") && !path.endsWith("/JavaTest") && !path.endsWith("/") && !path.contains("register") && !path.contains("registration") && !path.contains("home") && !path.contains("error") && !path.endsWith("css") && !path.endsWith("js") && session.getAttribute("currentCustomer") == null) {
            resp.sendRedirect("login.jsp");
        } else if (req.getMethod().equals("GET") && (path.endsWith("login")) || path.endsWith("registration")) {
            resp.sendRedirect(path + ".jsp");
        } else if ((path.contains("login") || path.contains("register") || path.contains("registration")) && session.getAttribute("currentCustomer") != null) {
            session.setAttribute("message", "You are already logged in!");
            resp.sendRedirect("error.jsp");
        } else if (path.endsWith("test.jsp") && session.getAttribute("questions") == null) {
            session.setAttribute("message", "You haven't choose test level");
            resp.sendRedirect("error.jsp");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        
    }
    
}

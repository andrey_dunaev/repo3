package com.luxoft.javatest.services;

import com.luxoft.javatest.exceptions.AuthorizationException;
import com.luxoft.javatest.model.Customer;
import java.util.Set;

public interface CustomerService {
    
    void saveCustomer(Customer customer);
    
    Customer getCustomer(String login);
    
    Customer getCustomer(String login, String password) throws AuthorizationException;
    
    Customer getCustomer(long id);
    
    Set<Customer> getAllCustomers();
    
    void changeCustomerRating(long id, int change);
    
}

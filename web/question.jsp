<%-- 
    Document   : login
    Created on : Nov 10, 2014, 1:24:20 PM
    Author     : ADunaev
--%>

<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
        <style>
            <%@include file="css/style.css" %>
        </style>
        <script>
            <%@include file="js/jquery-1.11.0.min.js" %>
            <%@include file="js/javatest.js" %>
        </script>
        <title>New question</title>
    </head>
    <body>
        <header>
            <div class="width">
                <h2>Java Test</h2>
                <div class="tagline">
                    <p>Test service for Java developers and students</p>
                </div>
            </div>
        </header>
        <section id="body" class="width clear">
            <aside id="sidebar" class="column-left">
                <c:choose>
                    <c:when test="${currentCustomer != null && currentCustomer.admin}">
                        <ul>
                            <li>
                                <h4>Administration</h4>
                                <ul class="blocklist">
                                    <li><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></li>
                                </ul>
                            </li>	
                        </ul>
                    </c:when>
                </c:choose>    
                <ul>
                    <li>
                        <h4>Navigate</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/home.jsp">Home</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/profile.jsp">My profile</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/test_choose.jsp">Tests</a></li>
                            <li class="selected-item"><a href="${pageContext.servletContext.contextPath}/question.jsp">Add question</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/rating">Rating</a></li>
                        </ul>
                    </li>	
                </ul>
                <ul>
                    <li>
                        <h4>System</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/login.jsp">Log in</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/registration.jsp">Registration</a></li>
                        </ul>
                    </li>	
                </ul>
            </aside>
            <section id="content" class="column-right">   
                <form id="form" action="addQuestion" method="POST">
                    <table id="table" style="border-width:2px; margin-left:180px; margin-top:50px; width:75%;">
                        <tr>
                            <th style="font-weight:bold; text-align:center; width:290pt;">Add new question</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td style="text-align:left;">Question text:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align:center;"><textarea name="questionText" id="questionText" maxlength="2048" rows="5" cols=50"></textarea></td>
                            <td id="questionError" class="error"></td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">Question options (check the right one):</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align:left;"><input type="radio" name="question" id="option1" value="1" style="margin-right:10px;"/><textarea name="option1" id="optionText1" maxlength="512" rows="1" cols="47"></textarea></td>
                            <td id="optionError1" class="error"></td>
                        </tr>
                        <tr>
                            <td style="text-align:left;"><input type="radio" name="question" id="option2" value="2" style="margin-right:10px;"/><textarea name="option2" id="optionText2" maxlength="512" rows="1" cols="47"></textarea></td>
                            <td id="optionError2" class="error"></td>
                        </tr>
                        <tr id="levelRow">
                            <td style="text-align:center;">Question level:
                                <select name="level" style="margin-left:10px;">
                                    <option selected value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </td>
                            <td id="notChosenError" class="error"></td>
                        </tr>
                        <tr>
                            <td style="text-align:center;"><input type="button" class="button" onclick="addOption();" value="Add option"/></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align:center;"><input type="submit" class="button" onclick="return checkQuestionForm();" value="Add question"/><input type="hidden" name="optionsCount" id="optionsCount" value="2"/></td>
                            <td></td>
                        </tr>
                    </table>
                </form>
            </section>
        </section>
    </body>
</html>

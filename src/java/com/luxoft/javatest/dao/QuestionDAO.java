package com.luxoft.javatest.dao;

import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.Question;
import java.util.List;

public interface QuestionDAO {
    
    void saveQuestion(Question question);
    
    void removeQuestion(Question question);
    
    void confirmQuestion(Question question);
    
    void checkAnsweredQuestion(Customer customer, Question question);
    
    Question getQuestionById(long id);
    
    List<Question> getQuestionsByCustomerId(long customerId); 
    
    List<Long> getAnsweredQuestions(long customerId);
    
    List<Question> getQuestionsForTest(long customerId, int level);
    
    List<Question> getConfirmedQuestions();
    
    List<Question> getTemporaryQuestions();
    
    List<Question> getAllQuestions();
    
    void changeQuestionRating(long id, int change);

}

<%-- 
    Document   : login
    Created on : Nov 10, 2014, 1:24:20 PM
    Author     : ADunaev
--%>

<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
        <style>
            <%@include file="css/style.css" %>
        </style>
        <script>
            <%@include file="js/jquery-1.11.0.min.js" %>
            <%@include file="js/javatest.js" %>
        </script>
        <title>My profile</title>
    </head>
    <body>
        <header>
            <div class="width">
                <h2>Java Test</h2>
                <div class="tagline">
                    <p>Test service for Java developers and students</p>
                </div>
            </div>
        </header>
        <section id="body" class="width clear">
            <aside id="sidebar" class="column-left">
                <c:choose>
                    <c:when test="${currentCustomer != null && currentCustomer.admin}">
                        <ul>
                            <li>
                                <h4>Administration</h4>
                                <ul class="blocklist">
                                    <li><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></li>
                                </ul>
                            </li>	
                        </ul>
                    </c:when>
                </c:choose>    
                <ul>
                    <li>
                        <h4>Navigate</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/home.jsp">Home</a></li>
                            <li class="selected-item"><a href="${pageContext.servletContext.contextPath}/profile.jsp">My profile</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/test_choose.jsp">Tests</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/question.jsp">Add question</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/rating">Rating</a></li>
                        </ul>
                    </li>	
                </ul>
                <ul>
                    <li>
                        <h4>System</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/login.jsp">Log in</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/registration.jsp">Registration</a></li>
                        </ul>
                    </li>	
                </ul>
            </aside>
            <section id="content" class="column-right">   
                <p style="margin-top: 20pt;">
                    <h4>My personal data</h4>
                </p>
                <p>Login: ${currentCustomer.login}</p>
                <p>Email: ${currentCustomer.email}</p>
                <p>Rating: ${currentCustomer.rating}</p>
                <c:choose>
                    <c:when test="${!currentCustomer.getQuestions().isEmpty()}">
                        <p>My questions: </p>
                        <c:forEach var="question" items="${currentCustomer.getQuestions()}" varStatus="status">
                            <p>${status.count}. ${question.content} ${question.temporary? "<input type=\"text\" style=\"border-width: 0; border-color: white; width: 30%; font-weight: bold; font-size: 1.0em;\" value=\"(question is on consideration)\"/>" : ""}
                                    <c:forEach var="option" items="${question.options}">
                                        <br><input type="radio" style="margin-right: 10px;" ${option.correct? "checked" : ""} disabled/>${option.content}
                                    </c:forEach>                      
                                    <br>Level: ${question.getLevelString()}
                                    <br>Rating: ${question.rating}
                            </p>
                        </c:forEach> 
                    </c:when>                
                </c:choose>
            </section>
        </section>
    </body>
</html>

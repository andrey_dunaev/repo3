package com.luxoft.javatest.model;

import java.util.ArrayList;
import java.util.List;

public class Question {
    
    private long id;
    private long authorId;
    private String content;
    private int level;
    private int rating;
    private boolean temporary = true;
    private List<Option> options = new ArrayList<Option>();
    
    public Question(String content, int level, int rating) {
        this.content = content;
        this.level = level;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLevel() {
        return level;
    }

    public String getLevelString() {
        if (level == 1) {
            return "basic";
        } else if (level == 2) {
             return "middle";
        } else {
            return "high";
        }
    }
    
    public void setLevel(int level) {
        this.level = level;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isTemporary() {
        return temporary;
    }

    public void setTemporary(boolean temporary) {
        this.temporary = temporary;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
    
    public Option getCorrectOption() {
        for (Option option: options) {
            if (option.isCorrect()) {
                return option;
            }
        }
        return null;
    }
    
    public void addOption(Option option) {
        options.add(option);
    }
    
}

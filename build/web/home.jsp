<%-- 
    Document   : test
    Created on : 09.11.2014, 20:05:07
    Author     : Lenovo
--%>

<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
        <style>
            <%@include file="css/style.css" %>
        </style>
        <script>
            <%@include file="js/jquery-1.11.0.min.js" %>
            <%@include file="js/javatest.js" %>
        </script>
        <title>Java Test</title>      
    </head>
    <body>
        <header>
            <div class="width">
                <h2>Java Test</h2>
                <div class="tagline">
                    <p>Test service for Java developers and students</p>
                </div>
            </div>
        </header>
        <section id="body" class="width clear">
            <aside id="sidebar" class="column-left">
                <c:choose>
                    <c:when test="${currentCustomer != null && currentCustomer.admin}">
                        <ul>
                            <li>
                                <h4>Administration</h4>
                                <ul class="blocklist">
                                    <li><a href="${pageContext.servletContext.contextPath}/admin">Admin page</a></li>
                                </ul>
                            </li>	
                        </ul>
                    </c:when>
                </c:choose>    
                <ul>
                    <li>
                        <h4>Navigate</h4>
                        <ul class="blocklist">
                            <li class="selected-item"><a href="${pageContext.servletContext.contextPath}/home.jsp">Home</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/profile.jsp">My profile</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/test_choose.jsp">Tests</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/question.jsp">Add question</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/rating">Rating</a></li>
                        </ul>
                    </li>	
                </ul>
                <ul>
                    <li>
                        <h4>System</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/login.jsp">Log in</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/registration.jsp">Registration</a></li>
                        </ul>
                    </li>	
                </ul>
            </aside>
            <section id="content" class="column-right">               									
                <h2 style="text-align: center;">Java Test Project</h2>            
                <p>
                    We are glad to see you at Java Test - world's number 1 testing service
                    for Java developers. On our site you can pass Java tests of three levels,
                    add questions for tests and thereby raise your Java rating. All abilities
                    of our service will be available to you after simple procedure of registration.
                    Best regards, Java Test Project team!
                </p>             
                <p>
                    Brief user instruction:
                </p>
                <p>
                    1. Register on our site.
                </p>
                <p>
                    2. Log in system.
                </p>
                <p>
                    3. Choose appropriate level test and pass it. 
                    You get 1 score point for every correctly answered basic level question,
                    2 points for middle level and 3 - for high level one.
                </p>
                <p>
                    4. You can add your question to test - just fill the corresponding form.
                    Don't forget to specify question level and correct option.
                    The question may be confirmed or rejected by our administration.
                    In case of question confirmation, you get 3, 6 or 9 points depending on
                    question level.
                </p>
                <p>
                    5. You will also get points from other users for questions, added by you.
                </p>
                <p>
                    6. View user's rating and find yourself in it!
                </p>
                <div style="text-align: center; font-size: 1.1em;">
                        <a href="${pageContext.servletContext.contextPath}/login.jsp">Log in system</a>
                        <div>
                            <br><a href="${pageContext.servletContext.contextPath}/registration.jsp">Register</a>
                        </div>
                </div>
            </section>
	</section>	
    </body>
</html>

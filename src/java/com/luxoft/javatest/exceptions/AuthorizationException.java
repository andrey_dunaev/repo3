package com.luxoft.javatest.exceptions;

public class AuthorizationException extends Exception {
    
    protected String message;
    
    public AuthorizationException(String message) {
        this.message = message;
    }
    
    @Override
    public String getMessage() {
        return message;
    }
    
}

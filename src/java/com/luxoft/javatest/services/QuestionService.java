package com.luxoft.javatest.services;

import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.Question;
import java.util.List;

public interface QuestionService {
    
    void saveQuestion(Customer customer, Question question);
    
    void confirmQuestion(Question question);
    
    void confirmQuestion(long id);
    
    void checkAnsweredQuestion(Customer customer, Question question);
    
    void removeQuestion(Question question);
    
    Question getQuestionById(long id);
    
    List<Question> getQuestionsByCustomerId(long customerId);
    
    List<Long> getAnsweredQuestions(long customerId);
       
    List<Question> getQuestionsForTest(long customerId, int level);
    
    List<Question> getConfirmedQuestions();
    
    List<Question> getTemporaryQuestions();
    
    List<Question> getAllQuestions();
    
    void addQuestions(List<Question> questions);
      
    void changeQuestionRating(long id, int change);
    
}

package com.luxoft.javatest.controllers;

import com.luxoft.javatest.model.Customer;
import com.luxoft.javatest.model.Option;
import com.luxoft.javatest.model.Question;
import com.luxoft.javatest.services.CustomerService;
import com.luxoft.javatest.services.QuestionService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class QuestionController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private QuestionService questionService;
    
    @RequestMapping(value = "checkResults")
    public ModelAndView checkResults(HttpServletRequest req) {
        Question question = null;
        Customer customer;
        String chosenOptionString;
        Option chosenOption;    
        Customer currentCustomer = (Customer)req.getSession().getAttribute("currentCustomer");
        long currentCustomerId = currentCustomer.getId();
        List<Question> questions = (List<Question>)req.getSession().getAttribute("questions");
        List<Customer> customers = (List<Customer>)req.getSession().getAttribute("customers");
        List<Long> answeredQuestions = (List<Long>)questionService.getAnsweredQuestions(currentCustomerId);
        int level = questions.get(0).getLevel();
        int result = 0;
        int plusRating = 0;
        int questionsSize = Integer.parseInt(req.getParameter("questionsSize"));
        for(int i = 0; i < questionsSize; i++) {
            question = questions.get(i);
            customer = customers.get(i);
            chosenOptionString = req.getParameter("question" + i);
            if (chosenOptionString != null) {
                chosenOption = question.getOptions().get(Integer.parseInt(chosenOptionString));
                chosenOption.setChosen(true);
                if (chosenOption.isCorrect()) {
                    result += level;
                    if (!answeredQuestions.contains(question.getId())) {
                        customerService.changeCustomerRating(currentCustomerId, level);
                        currentCustomer.setRating(currentCustomer.getRating() + level);
                        plusRating += level;
                        questionService.checkAnsweredQuestion(currentCustomer, question);
                    }
                }
            }           
            int ratingChange = Integer.parseInt(req.getParameter("ratingChange" + i));         
            questionService.changeQuestionRating(question.getId(), ratingChange);
            customerService.changeCustomerRating(customer.getId(), ratingChange);
        }
        req.setAttribute("result", result);
        req.setAttribute("plusRating", plusRating);
        return new ModelAndView("results.jsp");
    }
    
    @RequestMapping(value = "addQuestion")
    public ModelAndView addQuestion(HttpServletRequest req) {
        String content = req.getParameter("questionText");
        int level = Integer.parseInt(req.getParameter("level"));
        Question question = new Question(content, level, 0);
        int optionsCount = Integer.parseInt(req.getParameter("optionsCount"));
        int correctOptionNumber = Integer.parseInt(req.getParameter("question"));
        for (int i = 1; i <= optionsCount; i++) {
            Option option = null;
            content = req.getParameter("option" + i);
            if (i == correctOptionNumber) {
                option = new Option(content, true);
            } else {
                option = new Option(content);
            }
            question.addOption(option);
        }
        Customer customer = (Customer)req.getSession().getAttribute("currentCustomer");
        questionService.saveQuestion(customer, question);
        return new ModelAndView("question_added.jsp");
    }
    
    @RequestMapping(value = "test")
    public ModelAndView prepareTest(HttpServletRequest req) {
        Customer customer = (Customer)req.getSession().getAttribute("currentCustomer");
        int level = Integer.parseInt(req.getParameter("level"));
        req.getSession().setAttribute("level", level);
        List<Question> questions = new ArrayList<Question>();      
        List<Customer> customers = new ArrayList<Customer>();
        questions = questionService.getQuestionsForTest(customer.getId(), level);
        for (Question question: questions) {
            customers.add(customerService.getCustomer(question.getAuthorId()));
        }
        if (!questions.isEmpty()) {
            req.getSession().setAttribute("levelString", questions.get(0).getLevelString());
        }
        req.getSession().setAttribute("questions", questions);
        req.getSession().setAttribute("customers", customers);
        return new ModelAndView("test.jsp");
    }
    
}

<%-- 
    Document   : login
    Created on : Nov 10, 2014, 1:24:20 PM
    Author     : ADunaev
--%>

<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
        <style>
            <%@include file="css/style.css" %>
        </style>
        <script>
            <%@include file="js/jquery-1.11.0.min.js" %>
            <%@include file="js/javatest.js" %>
        </script>
        <title>Login page</title>
    </head>
    <body>
        <header>
            <div class="width">
                <h2>Java Test</h2>
                <div class="tagline">
                    <p>Test service for Java developers and students</p>
                </div>
            </div>
        </header>
        <section id="body" class="width clear">
            <aside id="sidebar" class="column-left">
                <ul>
                    <li>
                        <h4>Navigate</h4>
                        <ul class="blocklist">
                            <li><a href="${pageContext.servletContext.contextPath}/home.jsp">Home</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/profile.jsp">My profile</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/test_choose.jsp">Tests</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/question.jsp">Add question</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/rating">Rating</a></li>
                        </ul>
                    </li>	
                </ul>
                <ul>
                    <li>
                        <h4>System</h4>
                        <ul class="blocklist">
                            <li class="selected-item"><a href="${pageContext.servletContext.contextPath}/login.jsp">Log in</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/logout">Log out</a></li>
                            <li><a href="${pageContext.servletContext.contextPath}/registration.jsp">Registration</a></li>
                        </ul>
                    </li>	
                </ul>
            </aside>
            <section id="content" class="column-right">   
                <form id="form" action="login" method="POST">
                    <table style="border-width:2px; margin-left:250px; margin-top:80px;">
                        <tr>
                            <th style="font-weight:bold; text-align:center;" colspan="2">Enter your login and password</th>
                            <th hidden></th>
                        </tr>
                        <tr>
                            <td style="width: 70pt;">Login:</td>
                            <td style="text-align:center; width:100pt;"><input type="text" name="userLogin" id="login" style="width:90%; text-align:right;"/></td>
                            <td id="loginError" class="error"></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td style="text-align:center;"><input type="password" name="userPassword" id="password" style="width:90%; text-align:right;"/></td>
                            <td id="passwordError" class="error"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align:center;"><input type="submit" class="button" onclick="return checkLoginForm();" value="Log in"></td>
                            <td hidden></td>
                        </tr>
                    </table>
                    <div style="margin-left: 243pt; margin-top: 10pt;">
                        <a href="registration.jsp" style="font-size: 1.1em;">Register on site</a>
                    </div>
                </form>
            </section>
        </section>
    </body>
</html>

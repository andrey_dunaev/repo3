package com.luxoft.javatest.model;

import java.io.Serializable;
import java.util.List;

public class Customer implements Serializable {
    
    private long id;
    private String login;
    private String password;
    private String email;
    private int rating;
    private boolean admin;
    private List<Question> questions;
    
    public Customer(CustomerConfig config) {
        this.login = config.getLogin();
        this.password = config.getPassword();
        this.email = config.getEmail();
        this.rating = config.getRating();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
    
    public void addQuestion(Question question) {
        this.questions.add(question);
    }
    
}

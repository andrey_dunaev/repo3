package com.luxoft.javatest.services;

import com.luxoft.javatest.dao.CustomerDAO;
import com.luxoft.javatest.exceptions.AuthorizationException;
import com.luxoft.javatest.model.Customer;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {
    
    @Autowired
    private CustomerDAO customerDAO;
    
    @Override
    public void saveCustomer(Customer customer) {
        customerDAO.saveCustomer(customer);
    }
    
    @Override
    public Customer getCustomer(String name) {
        return customerDAO.getCustomer(name);
    }
    
    @Override
    public Customer getCustomer(String login, String password) throws AuthorizationException {
        return customerDAO.getCustomer(login, password);
    }
    
    @Override
    public Customer getCustomer(long id) {
        return customerDAO.getCustomer(id);
    }
    
    @Override
    public Set<Customer> getAllCustomers() {
        return customerDAO.getAllCustomers();
    }
    
    @Override
    public void changeCustomerRating(long id, int change) {
        customerDAO.changeCustomerRating(id, change);
    }
    
}
